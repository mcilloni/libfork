#  libfork, a base library for the Fork language
#  Copyright (C) Marco Cilloni <marco.cilloni@yahoo.com> 2014, 2015
#
#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.
#  Exhibit B is not attached; this software is compatible with the
#  licenses expressed under Section 1.12 of the MPL v2.


import err
import io
import mem
import txt


mut STREAM_IN uint8 = 1
mut STREAM_OUT uint8 = 2
mut STREAM_INOUT uint8 = 3


func streamClose(stream ptr io:Stream) bool
  mut ret = stream'cookie'cfn(stream'cookie'priv8, ptr stream'error)

  mem:free(stream)
  return ret
/func


func streamEnded(stream ptr io:Stream) bool
  return stream'eof
/func


func streamError(stream ptr io:Stream) ptr err:Error
  return stream'error
/func


func streamNew(priv8 data, rfn ptr io:readfunc, wfn ptr io:writefunc, cfn ptr io:closefunc) ptr io:Stream
  mut ret = cast<ptr io:Stream>(mem:zalloc(size(io:Stream)))
  ret'cookie'priv8 = priv8

  if wfn != null
    ret'inout = ret'inout | STREAM_OUT
  /if

  if rfn != null
    ret'inout = ret'inout | STREAM_IN
  /if

  ret'cookie'wfn = wfn
  ret'cookie'rfn = rfn
  ret'cookie'cfn = cfn
  return ret
/func


func streamRead(stream ptr io:Stream, string ptr uint8, len intptr) uintptr
  if (stream'inout & STREAM_IN) == 0
    stream'error = err:errorNew("Stream opened write-only")
    return 0
  /if

  return stream'cookie'rfn(stream'cookie'priv8, string, len, ptr stream'error, ptr stream'eof)
/func


func streamWrite(stream ptr io:Stream, string ptr uint8) uintptr
  if (stream'inout & STREAM_OUT) == 0
    stream'error = err:errorNew("Stream opened read-only")
    return 0
  /if

  return stream'cookie'wfn(stream'cookie'priv8, string, cast<intptr>(txt:strlen(string)), ptr stream'error)
/func
