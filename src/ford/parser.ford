#  libfork, a base library for the Fork language
#  Copyright (C) Marco Cilloni <marco.cilloni@yahoo.com> 2015
#
#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.
#  Exhibit B is not attached; this software is compatible with the
#  licenses expressed under Section 1.12 of the MPL v2.


module parser


import ast
import err
import list
import map


alias Context struct (
  fordPaths ptr list:List,
  imports ptr map:Map,
  currentFile ptr uint8,
  builtins ptr map:Map
)

decl ctxGetImport func(ctx ptr Context, name ptr uint8) ptr ast:PRoot
decl ctxFree func(ctx ptr Context)
decl ctxImport func(ctx ptr Context, importName ptr ast:StringDef, value ptr ptr ast:PNode) ptr list:List
decl ctxImportAll func(ctx ptr Context, imports ptr list:List) ptr list:List
decl ctxMatchBuiltin func(ctx ptr Context, name ptr uint8) ptr ast:Type
decl ctxNew func() ptr Context

decl ctxParseFile func(context ptr Context,
                       fileName ptr uint8,
                       newRoot ptr ptr ast:PNode) ptr list:List

decl ctxSetCurrentFile func(ctx ptr Context, filename ptr uint8)


alias Issue struct (
  gravity uint8,
  where ast:Line,
  filename,message ptr uint8
)


decl issueFree func(issue ptr Issue)
decl issueNew func(gravity uint8, where ast:Line, filename, message ptr uint8) ptr Issue
decl issueWriteOut func(issue ptr Issue, out ptr func(err ptr uint8))
decl issueToError func(issue ptr Issue) ptr err:Error


decl ISSUE_ERR uint8
decl ISSUE_WARN uint8
decl ISSUE_INFO uint8
