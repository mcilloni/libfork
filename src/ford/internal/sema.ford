#  libfork, a base library for the Fork language
#  Copyright (C) Marco Cilloni <marco.cilloni@yahoo.com> 2015
#
#  This Source Code Form is subject to the terms of the Mozilla Public
#  License, v. 2.0. If a copy of the MPL was not distributed with this
#  file, You can obtain one at http://mozilla.org/MPL/2.0/.
#  Exhibit B is not attached; this software is compatible with the
#  licenses expressed under Section 1.12 of the MPL v2.


module sema


import ast
import kv
import list
import parser
import utils


alias Resolver struct (
  ctx ptr parser:Context,
  root ptr ast:PRoot,
  aliases,decls ptr kv:KVList
)


decl resolverCollect func(rsv ptr Resolver,
                          ctx ptr parser:Context,
                          root ptr ast:PRoot,
                          errors ptr list:List) ptr kv:KVList

decl resolverDeinit func(rsv Resolver)


decl registerAlias func(rsv Resolver,
                        pdecl ptr ast:PDecl,
                        errors ptr list:List) ptr ast:Type

decl registerDecl func(rsv Resolver,
                       pdecl ptr ast:PDecl,
                       name ptr uint8,
                       type ptr ast:Type,
                       declType uint16,
                       errors ptr list:List) ptr ast:Type

decl registerTopDecl func(rsv Resolver,
                          pdecl ptr ast:PDecl,
                          errors ptr list:List) ptr ast:Type

decl resolveType func(rsv Resolver,
                      ptype ptr ast:PType,
                      errors ptr list:List) ptr ast:Type

decl validateExprRecl func(rsv Resolver,
                           pexpr ptr ast:PExpr,
                           recList,errors ptr list:List) ptr ast:Type

decl validateExpr func(rsv Resolver,
                       pexpr ptr ast:PExpr,
                       errors ptr list:List) ptr ast:Type

decl validateFunc func(rsv Resolver,
                       pdecl ptr ast:PDecl,
                       funcType ptr ast:Type,
                       errors ptr list:List) bool


decl semaValidateAstRoot func(ctx ptr parser:Context,
                              root ptr ast:PRoot,
                              errors ptr list:List) bool

# validates the AST, and returns a list of the issues found.
decl semaValidateAst func(context ptr parser:Context,
                          root ptr ast:PNode) ptr list:List

# ast sema operations

decl findAliasImport func(ctx ptr parser:Context, moduleName,name ptr uint8) ptr ast:Type
decl findAliasLocal func(node ptr ast:PNode, name ptr uint8) ptr ast:Type
decl findRecAlias func(ctx ptr parser:Context, scope ptr ast:PNode, ralias ptr ast:Type) ptr ast:Type

decl findSym func(node ptr ast:PNode, name ptr uint8) ptr ast:Type
decl findSymLocal func(node ptr ast:PNode, name ptr uint8) ptr ast:Type
decl findSymModule func(ctx ptr parser:Context, moduleName, name ptr uint8) ptr ast:Type
decl findStab func(node ptr ast:PNode) ptr ast:Symtable

decl typeIsAssignable func(ctx ptr parser:Context, scope ptr ast:PNode, dest,src ptr ast:Type) bool
decl typeIsCastable func(ctx ptr parser:Context, scope ptr ast:PNode, dest,src ptr ast:Type) bool
decl typeChooseLarger func(ctx ptr parser:Context, scope ptr ast:PNode, dest,src ptr ast:Type) bool
