libfork
=======

A simple implementation of basic routines, data structures and a full parser for the Fork
Language.

This is incredibly alpha, so it may crash in unpredictable ways.

Everything is available under the MPL v2 license.

*Building:* you need [first-step](http://github.com/mcilloni/first-step/) to build this, a C++ compiler (like clang or gcc) and GNU/Linux.
